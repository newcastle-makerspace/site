## Welcome

Newcastle Makerspace is at **21 Gordon St, Hamilton**, around the back, through the gate.

{{< leaflet-map mapHeight="500px" mapWidth="100%" mapLat="-32.9236835" mapLon="151.7523226" zoom=15 >}}
    {{< leaflet-marker markerLat="-32.9236835" markerLon="151.7523226" >}}
{{< /leaflet-map >}}

The space is open irregularly, often on Wednesday evenings, or Sunday mornings.

Meetings are announced on Discord, the mailing list, and via the Facebook Group.

### Contact and links

Our main method of communication is our [Discord Server](https://discord.gg/Mmz7gz2mfd).

The [Mailing List](http://newcastlelug.org/mailman/listinfo/makerspace_newcastlelug.org) isusually low-volume, with sporadic bursts of activity.

We also have a [Facebook Group](https://www.facebook.com/groups/627261683965099) that we post announcements and random things to.

All our codez go in our [Github repository](http://github.com/newcastlemakerspace) . Pull request away!