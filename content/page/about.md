---
title: About
comments: false
---

We are a Makerspace, (or Hackerspace). 

The basic concept is a not-for-profit communal space, that anyone can use, with access to communal tools and 
shared skills, to make or fix things. This might be for example, a sewing space, an electronics space, 
a woodworking space, a metal working space, a mechanical repairs/maintenance space etc.

Newcastle Makerspace is currently fairly electronics-focussed, but we're not about setting limits, 
and if you want to bring in other ideas/skills/tools, then feel free!

### History

The Makerspace was started in early 2014 after some online discussion and physical meetings late in 2013.